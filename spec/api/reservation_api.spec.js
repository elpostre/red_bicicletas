const request = require('request');
const { connect, connection, Types } = require('mongoose');

const server = require('../../bin/www');
const Reservation = require('../../models/reservation');
const User = require('../../models/user');
const Bycicle = require('../../models/bycicle');

describe('User controller', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });
  });

  afterEach((done) => {
    Reservation.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
    db.removeAllListeners();
  });

  describe('List Reservation', () => {
    it('should get a reservation list', (done) => {
      request.get('http://localhost:3000/api/reservations', (err, res, body) => {
        expect(res.statusCode).toBe(200);
        done();
      });
    });
  });

  describe('List Reservation By User', () => {
    let user;
    let reservation;
    let bycicle;

    beforeEach((done) => {
      const currentTime = new Date();
      const nextDay = new Date(currentTime.setDate(currentTime.getDate() + 1));
      user = new User({
        name: 'William Shakespeare'
      });
      bycicle = new Bycicle({
        code: 1,
        color: 'Silver',
        model: 'Mountain',
        location: [0.00, 0.00]
      });
      reservation = new Reservation({
        from: currentTime,
        to: nextDay,
        bycicle: bycicle._id,
        user: user._id
      });

      user.save();
      bycicle.save();
      reservation.save().then(() => {
        done();
      });
    });

    it('should get all reservations by user', (done) => {
      request.get(`http://localhost:3000/api/reservations/${user._id}`, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        expect(res.body).toEqual(JSON.stringify([reservation]));
        done();
      });
    });
  });

  describe('Create reservation', () => {
    let user;
    let bycicle;

    beforeEach((done) => {
      user = new User({
        name: 'William Shakespeare'
      });
      bycicle = new Bycicle({
        code: 1,
        color: 'Silver',
        model: 'Mountain',
        location: [0.00, 0.00]
      });

      user.save();
      bycicle.save().then(() => {
        done();
      });
    });

    it('should create a new reservation', (done) => {
      const currentTime = new Date();
      const nextDay = new Date(currentTime.setDate(currentTime.getDate() + 1));
      const body = JSON.stringify({
        from: currentTime,
        to: nextDay,
        bycicle: bycicle._id,
        user: user._id
      });

      request.post({
        headers:  {'Content-type': 'application/json'},
        url: `http://localhost:3000/api/reservations/create`,
        body: body
      }, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        expect(JSON.parse(res.body).user.name).toBe('William Shakespeare');
        expect(JSON.parse(res.body).bycicle.code).toBe(1);
        done();
      });
    });
  });
});
