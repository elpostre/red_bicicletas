const request = require('request');
const { connect, connection } = require('mongoose');

const Bycicle = require('../../models/bycicle');
const server = require('../../bin/www');

describe('Bycicle API', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });
  });

  afterEach((done) => {
    Bycicle.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });

    db.removeAllListeners();
  });


  describe('GET bycicles', () => {
    it('should handle 200 state', () => {
      request.get('http://localhost:3000/api/bycicles', (err, res, body) => {
        expect(res.statusCode).toBe(200);
      });
    });
  });

  describe('POST bycicle', () => {
    it('should handle a 200 state', (done) => {
      const headers = {'Content-type': 'application/json'};
      const byke = JSON.stringify({"id":2,"color":"orange","model":"race","latitude":0.00, "longitude": 0.00});

      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bycicles/create',
        body: byke
      }, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        done();
      });
    });
  });

  describe('DELETE bycicle', () => {
    it('should handle a 200 state', (done) => {
      const headers = {'Content-type': 'application/json'};
      const byke = JSON.stringify({"id":2,"color":"orange","model":"race","latitude":0.00, "longitude": 0.00});

      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bycicles/create',
        body: byke
      }, (err, res, body) => {});

      request.delete({
        headers: headers,
        url: 'http://localhost:3000/api/bycicles/delete',
        body: JSON.stringify({ id: 2 })
      }, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        done();
      });
    });
  });

  describe('PUT bycicle', () => {
    it('should handle a 200 state', (done) => {
      const headers = {'Content-type': 'application/json'};
      const byke = JSON.stringify({"id":2,"color":"orange","model":"race","latitude":0.00, "longitude": 0.00});

      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/bycicles/create',
        body: byke
      }, (err, res, body) => {});

      request.put({
        headers: headers,
        url: 'http://localhost:3000/api/bycicles/update',
        body: JSON.stringify({ id: 2, "color":"beige","model":"mountain","latitude":9.99, "longitude": 9.99 })
      }, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        expect(JSON.parse(res.body).bycicle.code).toBe(2);
        expect(JSON.parse(res.body).bycicle.color).toBe('beige');
        expect(JSON.parse(res.body).bycicle.model).toBe('mountain');
        expect(JSON.parse(res.body).bycicle.location).toEqual([9.99, 9.99]);
        done();
      });
    });
  })
});