const request = require('request');
const { connect, connection } = require('mongoose');

const server = require('../../bin/www');
const User = require('../../models/user');

describe('User controller', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
    db.removeAllListeners();
  });

  describe('List Users', () => {
    it('should return a list of users', (done) => {
      request.get('http://localhost:3000/api/users', (err, res, body) => {
        expect(res.statusCode).toBe(200);
        done();
      });
    });
  });

  describe('Add User', () => {
    it('should create a new user', (done) => {
      const headers = {'Content-type': 'application/json'};
      const user = JSON.stringify({name: 'William Shakespeare'});

      request.post({
        headers: headers,
        url: 'http://localhost:3000/api/users/create',
        body: user
      }, (err, res, body) => {
        expect(res.statusCode).toBe(200);
        done();
      });
    });
  });
});