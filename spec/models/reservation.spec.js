const { connect, connection, Types } = require('mongoose');

const Bycicle = require('../../models/bycicle');
const User = require('../../models/user');
const Reservation = require('../../models/reservation');

describe('Reservation Model', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;
  let user;
  let bycicle;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });

    user = new User({_id: new Types.ObjectId(), name : 'William Shakespeare'});
    bycicle = new Bycicle({_id: new Types.ObjectId(), code: 1, color: 'blue', model: 'classic', location: [-34.5, -54.1]});
    user.save();
    bycicle.save();
  });

  afterEach((done) => {
    Reservation.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
    db.removeAllListeners();
  });

  it('should create a new reservation', () => {
    const currentTime = new Date();
    const nextDay = new Date(currentTime.setDate(currentTime.getDate() + 1));

    const reservation = new Reservation({
      from: currentTime,
      to: nextDay,
      bycicle: bycicle._id,
      user: user._id
    });

    expect(reservation.from).toEqual(currentTime);
    expect(reservation.to).toEqual(nextDay);
    expect(reservation.bycicle).toEqual(bycicle._id);
    expect(reservation.user.toString()).toEqual(user.id.toString());
  });

  it('should save a new reservation', (done) => {
    const currentTime = new Date();
    const nextDay = new Date(currentTime.setDate(currentTime.getDate() + 1));

    const reservation = new Reservation({
      from: currentTime,
      to: nextDay,
      bycicle: bycicle._id,
      user: user._id
    });

    expect(reservation.from).toEqual(currentTime);
    expect(reservation.to).toEqual(nextDay);
    expect(reservation.bycicle).toEqual(bycicle._id);
    expect(reservation.user.toString()).toEqual(user.id.toString());

    reservation.save().then(() => {
      Reservation.find({}).populate('bycicle').populate('user').exec((err, res) => {
        expect(res[0].from).toEqual(currentTime);
        expect(res[0].to).toEqual(nextDay);
        expect(res[0].bycicle.code).toEqual(1);
        expect(res[0].user.name).toEqual('William Shakespeare');
        done();
      });
    });
  });
});