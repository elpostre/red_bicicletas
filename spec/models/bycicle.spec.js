const { connect, connection } = require('mongoose');

const Bycicle = require('../../models/bycicle');

describe('Bycicles Model', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });
  });

  afterEach((done) => {
    Bycicle.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
    db.removeAllListeners();
  });

  describe('Bycicle create instance', () => {
    it('should create a new bycicle', () => {
      const bike = new Bycicle({code: 1, color:'green', model:'road', location:[-34.5, -54.1]});

      expect(bike.code).toBe(1);
      expect(bike.color).toBe('green');
      expect(bike.model).toBe('road');
      expect(bike.location).toEqual([-34.5, -54.1]);
    });

    it('should return an empty bycicles collection', (done) => {
      Bycicle.find({}, (err, bycicles) => {
        expect(bycicles.length).toBe(0);
        done();
      });
    });

    it('should add a bycicle', (done) => {
      const bycicle = new Bycicle({code: 1, color:'green', model:'road', location:[-34.5, -54.1]});

      bycicle.save().then(() => {
        Bycicle.find({}, (err, bycicles) => {
          expect(bycicles.length).toBe(1);
          expect(bycicles[0].code).toBe(1);
          done();
        });
      });
    });

    it('should find a bycicle by code', (done) => {
      const bycicle = new Bycicle({code: 20, color:'red', model:'mountain', location:[-99.5, -98.1]});

      bycicle.save().then(() => {
        Bycicle.findOne({code: 20}, (err, res) => {
          expect(res.code).toBe(bycicle.code);
          expect(res.color).toBe(bycicle.color);
          expect(res.model).toBe(bycicle.model);
          expect(res.location).toEqual(bycicle.location);
          done();
        });
      });
    });

    it('should remove one bycicle', (done) => {
      const bycicle1 = new Bycicle({code: 1, color:'green', model:'road', location:[-34.5, -54.1]});
      const bycicle2 = new Bycicle({code: 20, color:'red', model:'mountain', location:[-99.5, -98.1]});

      bycicle1.save().then(() => {
        bycicle2.save().then(() => {
          Bycicle.deleteOne({code: 1}, (err, res) => {
            Bycicle.find({}, (err, bycicles) => {
              expect(bycicles.length).toBe(1);
              expect(bycicles[0].code).toBe(20);
              done();
            });
          });
        });
      });      
    });
  });
});