const { connect, connection } = require('mongoose');

const User = require('../../models/user');

describe('User Model', () => {
  const mongoDB = 'mongodb://localhost/testbd';
  const db = connection;

  beforeEach((done) => {
    connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});
    db.on('error', (err) => console.error(error, 'connection error'));
    db.once('open', () => {
      console.log('Connected to the database');
      done();
    });
  });

  afterEach((done) => {
    User.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
    db.removeAllListeners();
  });

  describe('User create instance', () => {
    it('should create a new user', () => {
      const user = new User({name: 'Carlos Avila'});

      expect(user.name).toBe('Carlos Avila');
    });

    it('should return an empty users collection', (done) => {
      User.find({}, (err, users) => {
        expect(users.length).toBe(0);
        done();
      });
    });

    it('should add a new user', (done) => {
      const user = new User({name: 'Andy Warhol'});

      user.save().then(() => {
        User.find({}, (err, users) => {
          expect(users.length).toBe(1);
          expect(user.name).toBe('Andy Warhol');
          done();
        });
      });
    });
  });
});