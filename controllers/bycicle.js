const Bycicle = require('../models/bycicle');

exports.bycicle_list = (req, res) => {
  Bycicle.find({}, (err, bycicles) => {
    if(err) {
      res.status(500);
    } else {
      res.render('bycicles/index', {bycicles: bycicles})
    }
  });
};

exports.bycicle_create_get = (req, res) => {
  res.render('bycicles/create');
};

exports.bycicle_create_post = (req, res) => {
  const { id, color, model, latitude, longitude } = req.body;
  const bycicle = new Bycicle({code: id, color: color, model: model, location: [latitude, longitude]});

  bycicle.save().then((bycicle) => {
    if (bycicle) {
      res.redirect('/bycicles');
    } else {
      res.redirect('/bycicles').status(500);
    }
  });
  
}

exports.bycicle_delete = (req, res) => {
  const id = req.body.id;

  Bycicle.deleteOne({code: id}, (err, bycicle) => {
    if(bycicle)  {
      res.redirect('/bycicles');
    } else {
      res.status(500);
    }
  });
}

exports.bycicle_update_get = (req, res) => {
  const id = req.params.id;

  Bycicle.findOne({code: id}, (err, bycicle) => {

    if(err)  {
      res.status(500);
    } else {
      res.render('bycicles/update', {bycicle: bycicle});
    }
  });

  
}

exports.bycicle_update_post = (req, res) => {
  const { id, color, model, latitude, longitude } = req.body; 

  Bycicle.findOneAndUpdate({code: id}, {code: id, color: color, model: model, location: [latitude, longitude]}, (err, bycicle) => {
    if(err)  {
      res.status(500);
    } else {
      res.render('bycicles/update', {bycicle: bycicle});
    }
  });
  

  res.redirect('/bycicles');
}
