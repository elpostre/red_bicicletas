const crypto = require('crypto');

const User = require('../models/user');
const Token = require('../models/token');
const Mailer = require('../mailer/mailer');


exports.list = (req, res) => {
  User.find({}, (err, users) => {
    if (err) {
      res.status(500);
    } else {
      res.render('users/', {users: users});
    }
  });
};

exports.update_get = (req, res) => {
  User.findById(req.params.id, (err, user) => {
    res.render('users/update', {errors: {}, user: user});
  });
};

exports.update_post = (req, res) => {
  User.findByIdAndUpdate(req.body.id, {name: req.body.name}, (err, user) => {
    if (err || !user) {
      const errors = err ? err.errors : {errors: {name: {message: 'Cannot Find User'}}};
      res.render(`/users/${req.params.id}/update`, {errors: errors, user: new User({name: req.body.name, email: req.body.email})})
    } else {
      res.redirect('/users');
      return;
    }

  });
};

exports.create_get = (req, res) => {
  res.render('users/create', {errors: {}, user: new User()});
};

exports.create = (req, res) => {
  if (req.body.password !== req.body.confirm_password) {
    res.render('users/create', {errors: {confirm_password: {message:`Password and confirm password must match`}}, user: new User({name: req.body.name, email: req.body.email})});
    return;
  }

  const user = new User({name: req.body.name, email: req.body.email, password: req.body.password});

  user.save((err) => {
    if (err) {
      res.render('users/create', {errors: err.errors, user: new User({name: req.body.name, email: req.body.email})});
    } else {
      const token = new Token({_userId: user.id, token: crypto.randomBytes(16).toString('hex'), createdAt: new Date()});

      token.save((err, token,) => {
        if (!err) {
          Mailer(user.email, token);
          res.redirect('/users');
        }
      });
    }
  });
};

exports.delete = (req, res, next) => {
  User.findByIdAndDelete(req.body.id, (err) => {
    if (err) {
      next(err);
    } else {
      res.redirect('/users');
    }
  });
};