const Reservation = require('../../models/reservation');
const Bycicle = require('../../models/bycicle');
const User = require('../../models/user');

exports.create_reservation = (req, res) => {
  User.findById(req.body.user, (err, user) => {
    Bycicle.findById(req.body.bycicle, (err, bycicle) => {
      console.log(user._id, bycicle._id)
      const reservation = new Reservation({
        from: new Date(req.body.from),
        to: new Date(req.body.to),
        bycicle: bycicle._id,
        user: user._id
      });

      reservation.save().then((error, rsv) => {
        Reservation.findById(reservation._id).populate('bycicle').populate('user').exec((err, reservation) => {
          if (err) {
            res.status(500);
          } else {
            res.status(200).json(reservation);
          }
        });
      });
    });
  });  
};

exports.get_all_reservations = (req, res) => {
  Reservation.find({}, (err, reservations) => {
    if (err) {
      res.status(500);
    } else {
      res.status(200).json(reservations);
    }
  });
};

exports.get_reservation_by_user = (req, res) => {
  User.findById(req.params.id, (err, user) => {
    Reservation.find({user: user._id}, (err, reservation) => {
      if (err) {
        res.status(500);
      } else {
        res.status(200).json(reservation);
      }
    });
  });
};