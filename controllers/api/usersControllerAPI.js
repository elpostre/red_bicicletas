const bcrypt =require('bcrypt');

const User = require('../../models/user');
const Mailer = require('../../mailer/mailer');

const saltRounds = 10;

exports.users_list = (req, res) => {
  User.find({}, (err, users) => {
    if (err) {
      res.status(500);
    } else {
      res.status(200).json({
        users: users
      });
    }
  });
};

exports.users_create = (req, res) => {
  const user = new User({name: req.body.name});

  User.pre('save', (next) => {
    if (user.isModified('password')) {
      user.password = bcrypt.hashSync(user.password, saltRounds);
    }
    next();
  });

  user.save((err) => {
    if (err) {
      res.status(500);
    } else {
      Mailer(user.email);
      res.status(200).json(user);
    }
  });
};