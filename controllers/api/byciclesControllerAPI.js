const Bycicle = require('../../models/bycicle');

exports.bycicles_list = (req, res) => {
  Bycicle.find({}, (err, bycicles) => {
    if(err) {
      res.status(500).json({
        error: err
      });
    } else {
      res.status(200).json({
        bycicles: bycicles
      });
    }
  });
  
};

exports.bycicle_create = (req, res) => {
  const {id, color, model, latitude, longitude} = req.body;
  const bike = new Bycicle({code: id, color: color, model: model, location:[latitude, longitude]});
  
  bike.save().then((bycicle) => {
    if (bycicle) {
      res.status(200).json({
        bycicle: bycicle
      });
    } else {
      res.status(500).json({
        error: err
      });
    }
  });
};

exports.bycicle_delete = (req, res) => {
  const id = req.body.id;

  Bycicle.deleteOne({code: id}, (err, bycicle) => {
    if(err)  {
      res.status(500).json({
        error: err
      });
    } else {
      res.status(200).send();
    }
  });
  
};

exports.bycicle_edit = (req, res) => {
  const {id, color, model, latitude, longitude} = req.body;

  Bycicle.findOneAndUpdate({code: id}, {code: id, color: color, model: model, location: [latitude, longitude]}, {new: true}, (err, bycicle) => {

    if(err)  {
      res.status(500).json({
        error: err
      });
    } else {
      res.status(200).json({
        bycicle: bycicle
      });
    }
  });
}
