const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../../models/user');

module.exports = {
  authenticate: (req, res, next) => {
    User.findOne({email: req.body.emal}, (err, user) => {
      if (err) {
        next(err);
      } else {
        if (user ===null) {
          return res.status(401).json({status: 'error', message: 'Invalid email or password', data: null});
        }

        if (user !== null && bcrypt.compareSync(req.body.password, user.password)) {
          user.save((err, usr) => {
            const token = jwt.sign({id: usr._id}, req.app.get('secretKey'), {expiresIn: '7d'});
            res.status(200).json({message: 'user found', data: {user: user, token: token}});
          });
        } else {
          res.status(401).json({status: 'error', message: 'Invalid email or password', data: null})
        }
      }
    });
  },
  forgotPassword: (req, res, next) => {
    User.findOne({email: req.body.email}, (err, user) => {
      if (!user) return res.status(401).json({message: 'User does not exist', data: null});

      user.resetPassword((err) => {
        if(err) return next(err);
        res.status(200).json({message: 'An email was sended to your account with the instructions to reset your password', data: null});
      });
    });
  }
}
