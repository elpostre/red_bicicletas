const passport = require('../config/passport');

exports.login = (req, res) => {
  res.render('session/login');
};

exports.login_post = (req, res, next) => {
  passport.authenticate('local', function(err, user, info) {
    if(err) return next(err);
    if(!user) return res.render('session/login', {info: info});
    req.login(user, (err) => {
      if(err) return next(err);

      return res.redirect('/');
    });
  })(req, res, next);
};

exports.logout = (req, res) => {
  req.logout();
  res.redirect('/');
};