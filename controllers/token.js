const Token = require('../models/token');
const User = require('../models/user');

exports.confirm = (req, res, next) => {
  Token.findOne({token: req.params.token}, (err, token) =>{
    if(!token) return res.status(404).send({type: 'not-verified', msg: `There's not user associated to this token, maybe it has expired. request a new one`});

    User.findByIdAndUpdate(token._userId, {verified: true}, (err, user) => {
      if (err) return res.status(500).send({msg: err.message});
      if (!user) return res.status(400).send({msg: `Can't find this user`});

      res.redirect('/');
    });
  })
}