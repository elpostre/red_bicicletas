var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var session = require('express-session');
var jwt = require('jsonwebtoken');

var passport = require('./config/passport');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tokenRouter = require('./routes/token');
var loginRouter = require('./routes/login');
var byciclesRouter = require('./routes/bycicles');
var byciclesAPIRouter = require('./routes/api/bycicles');
var usersAPIRouter = require('./routes/api/users');
var authAPIRouter = require('./routes/api/auth');
var reservationsAPIRouter = require('./routes/api/reservations');

var mongoDB = 'mongodb://localhost/bycicles_network';
var store = new session.MemoryStore;
var app = express();

app.use(session({
  cookie: {maxAge: 240 * 60 * 60 * 1000},
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: 'bycicles_network'
}));

mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false });
mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error', (e) => console.log('MongoDB connection error'));

app.set('secretKey', 'jwt_pwd_11223344');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/token', tokenRouter);
app.use('/bycicles', userLoggedIn, byciclesRouter);
app.use('/session', loginRouter);
app.use('/api/bycicles', validateUserAPI, byciclesAPIRouter);
app.use('/api/users', usersAPIRouter);
app.use('/api/reservations', reservationsAPIRouter);
app.use('/api/auth', authAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const userLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.redirect('/login');
  }
};

const validateUserAPI = (req, res, next) => {
  jwt.verify(req.headers['x-access-token'], req.app.get('accessKey'), (err, decoded) => {
    if (err) {
      res.json({status: 'error', message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log('jwt veritfied' + decoded);
      next();
    }
  })
}

module.exports = app;
