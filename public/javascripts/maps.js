var mymap = L.map('mapid').setView([51.52, -0.07], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  accessToken: 'pk.eyJ1IjoiY2F2aWxhcnRzIiwiYSI6ImNrYWc2bmdmczAxN3UyeW11bWxqdzhzNzYifQ.ctsaaTWVULviPfClA5FIpw'
}).addTo(mymap);

function getBycicles() {
  fetch('http://localhost:3000/api/bycicles')
    .then(res => res.json())
    .then(res => {
      res.bycicles.forEach((b) => {
        L.marker(b.location, {title: b.id}).addTo(mymap);
      })
    });
}
// L.marker([51.5574439,0.0709351]).addTo(mymap);
// L.marker([51.5210467,-0.0738255]).addTo(mymap);
// L.marker([51.4769128,-0.0007244]).addTo(mymap);

getBycicles();
