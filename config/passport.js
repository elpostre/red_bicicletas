const passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;

const User = require('../models/user');

passport.use(new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password'
  },
  (email, password, done) => {
    User.findOne({email: email}, (err, user) => {
      
      if (err) return done(err);
      if (!user) return done(null, false, {message: `Can't find this email`});
      user.validPassword(password, (res) => {
        if (!res) {
          return done(null, false, {message: `Wrong Password`});
        };
        return done(null, user);
      });
    });
  }
));
passport.serializeUser((user, cb) => {
  cb(null, user.id);
});
passport.deserializeUser((id, cb) => {
  User.findById(id, (err, user) => {
    cb(err, user);
  });
});

module.exports = passport;