const { Schema, model } = require('mongoose');

const TokenSchema = new Schema({
  _userId: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  token: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    required: true,
    expires: 43200
  }
});

module.exports = model('Token', TokenSchema);
