const { model, Schema } = require('mongoose');

const reservationSchema = new Schema({
  from: Date,
  to: Date,
  bycicle: {
    type: Schema.Types.ObjectId, ref: 'Bycicle'
  },
  user: {
    type: Schema.Types.ObjectId, ref: 'User'
  }
});

module.exports = model('Reservation', reservationSchema);