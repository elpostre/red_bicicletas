const { model, Schema } = require('mongoose');
const UniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

const Token = require('./token');
const Mailer = require('../mailer/mailer');

const saltRounds = 10;

const re =  /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const validateEmail = (email) => {
  return re.test(email);
};

const userSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Name is mandatory']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'Email is mandatory'],
    lowercase: true,
    validate: [validateEmail, 'This Email Address is not valid'],
    match: [re],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Password is mandatory'],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verified: {
    type: Boolean,
    default: false
  }
});

userSchema.plugin(UniqueValidator, {message: '{PATH} already exist'});

userSchema.pre('save', function (next, docs) {
  this.password = bcrypt.hashSync(this.password, saltRounds);
  next();
});

userSchema.methods.validPassword = function (pass, cb) {
  bcrypt.compare(pass, this.password, (err, isMatch) => {
      result = isMatch;
      if (err) cb(err);
      return cb(isMatch);
  });
};

userSchema.methods.resetPassword = (cb) => {
  const token = new token({_userId: this.id, token: crypto.randomBytes(16).randomBytes('hex')});
  const email_destination = this.email;
  token.save((err) => {
    if (err) return cb(err);

    Mailer.sendMailToken({
      from: 'no-reply@byciclesnetwork.com',
      to: email_destination,
      subject: 'Password Reset',
      text: `Hi enter to this address to reset your password http://locakhost:3000/resetPassword/${token.token}/`
    });

    cb(null);
  });
};

module.exports = model('User', userSchema);
