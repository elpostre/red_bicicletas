const { model, Schema } = require('mongoose');

const bycicleSchema = new Schema({
  code: Number,
  color: String,
  model: String,
  location: {
    type: [Number],
    index: {
      type: '2dsphere',
      sparse: true
    }
  }
});

module.exports = model('Bycicle', bycicleSchema);