const nodemailer = require('nodemailer');

const main = async (user, token) => {
  let transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
      user: 'samantha17@ethereal.email',
      pass: 'vbpEZm6YecEggXP2vJ'
    }
  });

  let info = await transporter.sendMail({
    from: 'Bycicles network <info@byciclesnetwork.io>',
    to: user,
    text: 'You are now registered to bycicles network',
    html: `<p>You are now registered to bycicles network</p> click to activate <a href="http://localhost:3000/token/confirmation/${token.token}">`
  });

  console.log("Message sent: %s", info.messageId);

  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
};

module.exports = main;