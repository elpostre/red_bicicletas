const express = require('express');

const bycicleCotroller = require('../controllers/bycicle');

const router = express.Router();

router.get('/', bycicleCotroller.bycicle_list);
router.get('/create', bycicleCotroller.bycicle_create_get);
router.post('/create', bycicleCotroller.bycicle_create_post);
router.post('/delete', bycicleCotroller.bycicle_delete);
router.get('/:id/update', bycicleCotroller.bycicle_update_get);
router.post('/update', bycicleCotroller.bycicle_update_post);

module.exports = router;