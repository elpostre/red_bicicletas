var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users');

/* GET users listing. */
router.get('/', usersController.list);
router.get('/create', usersController.create_get);
router.post('/create', usersController.create);
router.get('/:id/update', usersController.update_get);
router.post('/update', usersController.update_post);
router.post('/delete', usersController.delete);

module.exports = router;
