const express = require('express');

const authController = require('../../controllers/api/authControllerAPI');

const router = express.Router();

router.post('/authenticate', authController.authenticate);
router.post('/forgotPassword', authController.forgotPassword);

module.exports = router;
