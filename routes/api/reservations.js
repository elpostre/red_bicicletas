const express = require('express');

const reservationsControllerAPI = require('../../controllers/api/reservationsControllerAPI');

const router = express.Router();

router.get('/', reservationsControllerAPI.get_all_reservations);
router.get('/:id', reservationsControllerAPI.get_reservation_by_user);
router.post('/create', reservationsControllerAPI.create_reservation);

module.exports = router;
