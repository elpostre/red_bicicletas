const express = require('express');

const byciclesControllerAPI = require('../../controllers/api/byciclesControllerAPI');

const router = express.Router();

router.get('/', byciclesControllerAPI.bycicles_list);
router.post('/create', byciclesControllerAPI.bycicle_create);
router.put('/update', byciclesControllerAPI.bycicle_edit);
router.delete('/delete', byciclesControllerAPI.bycicle_delete);

module.exports = router;
