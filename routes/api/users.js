const express = require('express');

const usersControllerAPI = require('../../controllers/api/usersControllerAPI');

const router = express.Router();

router.get('/', usersControllerAPI.users_list);
router.post('/create', usersControllerAPI.users_create);

module.exports = router;
