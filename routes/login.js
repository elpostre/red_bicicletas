var express = require('express');
var router = express.Router();
var sessionController = require('../controllers/login');

router.get('/login', sessionController.login);

router.post('/login', sessionController.login_post);

router.get('/logout', sessionController.logout);

router.get('forgotPassword', (req, res) => {});

router.post('forgotPassword', (req, res) => {});


module.exports = router;
